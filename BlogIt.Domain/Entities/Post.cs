﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogIt.Domain.Entities
{
    [Table("Post")]
    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}