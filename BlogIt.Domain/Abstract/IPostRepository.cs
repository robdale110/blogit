﻿using System.Collections.Generic;
using BlogIt.Domain.Entities;

namespace BlogIt.Domain.Abstract
{
    public interface IPostRepository
    {
        IEnumerable<Post> Posts { get; }
    }
}