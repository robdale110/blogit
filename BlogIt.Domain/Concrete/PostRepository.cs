﻿using System.Collections.Generic;
using BlogIt.Domain.Abstract;
using BlogIt.Domain.Entities;

namespace BlogIt.Domain.Concrete
{
    public class PostRepository : IPostRepository
    {
        private BlogItContext _blogItContext = new BlogItContext();

        public IEnumerable<Post> Posts
        {
            get { return _blogItContext.Posts; }
        }
    }
}
