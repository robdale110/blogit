﻿using System.Data.Entity;
using BlogIt.Domain.Entities;

namespace BlogIt.Domain.Concrete
{
    public class BlogItContext : DbContext
    {
        public DbSet<Post> Posts { get; set; }
    }
}
