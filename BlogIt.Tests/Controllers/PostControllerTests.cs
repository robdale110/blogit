﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BlogIt.Domain.Abstract;
using BlogIt.Domain.Entities;
using BlogIt.WebUi.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BlogIt.Tests.Controllers
{
    [TestClass]
    public class PostControllerTests
    {
        private Mock<IPostRepository> GetMockPostRepository()
        {
            var mockRepository = new Mock<IPostRepository>();
            mockRepository.Setup(m => m.Posts).Returns(new List<Post>
            {
                new Post { PostId = 1, Title = "First post", Body = "This is some text for the post" },
                new Post { PostId = 2, Title = "Second post", Body = "This is some text for the post" },
                new Post { PostId = 3, Title = "Third post", Body = "This is some text for the post" },
                new Post { PostId = 4, Title = "Second post", Body = "This is some text for the post" },
                new Post { PostId = 5, Title = "Third post", Body = "This is some text for the post" }
            });
            return mockRepository;
        }

        [TestMethod]
        public void Can_List_All_Posts()
        {
            // Arrange
            var mock = GetMockPostRepository();
            var target = new PostController(mock.Object);

            // Act
            var result = target.List();

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(mock.Object.Posts.Count(), 5);
        }
    }
}
