﻿/// <vs BeforeBuild='default' />
// Include plug-ins
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var del = require('del');
var minifyCSS = require('gulp-minify-css');
var copy = require('gulp-copy');
var bower = require('gulp-bower');
var sourcemaps = require('gulp-sourcemaps');

var config = {
    // Javascript file sto be bundled
    jquerysrc: [
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/jquery-validation/dist/jquery.validate.min.js',
        'bower_components/jquery-validation-unobtrusive/jquery.validate.unobtrusive.min.js'
    ],
    jquerybundle: 'Scripts/jquery-bundle.min.js',

    // Javascript file sthat will be combined into a bootstrap bundle
    bootstrapsrc: [
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/respond-minmax/dest/respond.min.js'
    ],
    bootstrapbundle: 'Scripts/bootstrap-bundle.min.js',

    // Modernizr javascript files
    modernizrsrc: ['bower_components/modernizr/modernizr.js'],
    modernizrbundle: 'Scripts/modernizr.min.js',

    // Add bundle for angular
    angularsrc: ['bower_components/angular/angular.min.js'],
    angularbundle: 'Scripts/angular.min.js',

    // CSS and font files for bootstrap
    bootstrapcss: 'bower_components/bootstrap/dist/css/bootstrap.css',
    bootstrapfonts: 'bower_components/bootstrap/dist/fonts/*.*',

    appcss: 'Content/Site.css',
    fontsout: 'Content/dist/fonts',
    cssout: 'Content/dist/css'
}

// Delete output script files
gulp.task('clean-vendor-scripts', function (cb) {
    del([config.jquerybundle, config.bootstrapbundle, config.modernizrbundle, config.angularbundle], cb);
});

// Create a jquery bundled file for minified scripts
gulp.task('jquery-bundle', ['clean-vendor-scripts', 'bower-restore'], function () {
    return gulp.src(config.jquerysrc)
        .pipe(sourcemaps.init())
        .pipe(concat('jquery-bundle.min.js'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('Scripts'));
});

// Create bootstrap bundled javascript
gulp.task('bootstrap-bundle', ['clean-vendor-scripts', 'bower-restore'], function () {
    return gulp.src(config.bootstrapsrc)
        .pipe(sourcemaps.init())
        .pipe(concat('bootstrap-bundle.min.js'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('Scripts'));
});

// Create modernizr bundled javascript
gulp.task('modernizr', ['clean-vendor-scripts', 'bower-restore'], function () {
    return gulp.src(config.modernizrsrc)
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(concat('modernizr.min.js'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('Scripts'));
});

// Create angular bundled javascript
gulp.task('angular-bundle', ['clean-vendor-scripts', 'bower-restore'], function() {
    return gulp.src(config.angularsrc)
        .pipe(sourcemaps.init())
        .pipe(concat('angular.min.js'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('Scripts'));
});

// Delete output files from css and fonts
gulp.task('clean-styles', function (cb) {
    del([config.fontsout, config.cssout], cb);
});

gulp.task('css', ['clean-styles', 'bower-restore'], function () {
    return gulp.src([config.bootstrapcss, config.appcss])
         .pipe(concat('app.css'))
         .pipe(gulp.dest(config.cssout))
         .pipe(minifyCSS())
         .pipe(concat('app.min.css'))
         .pipe(gulp.dest(config.cssout));
});

gulp.task('fonts', ['clean-styles', 'bower-restore'], function () {
    return gulp.src(config.bootstrapfonts)
        .pipe(gulp.dest(config.fontsout));
});

// Combine and minify css files and output fonts
gulp.task('styles', ['css', 'fonts'], function () {

});

// Combine the vendor files from bower into bundles
gulp.task('vendor-scripts', ['jquery-bundle', 'bootstrap-bundle', 'modernizr', 'angular-bundle'], function () {

});

gulp.task('bower-restore', function () {
    return bower();
});

gulp.task('default', ['vendor-scripts', 'styles'], function () { });