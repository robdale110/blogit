﻿using System.Web.Mvc;
using BlogIt.Domain.Abstract;

namespace BlogIt.WebUi.Controllers
{
    public partial class PostController : Controller
    {
        private IPostRepository _postRepository;

        public PostController(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        public virtual ViewResult List()
        {
            return View(_postRepository.Posts);
        }
    }
}