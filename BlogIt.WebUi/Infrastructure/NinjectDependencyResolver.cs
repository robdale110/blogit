﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BlogIt.Domain.Abstract;
using BlogIt.Domain.Concrete;
using Ninject;

namespace BlogIt.WebUi.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel _kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            _kernel.Bind<IPostRepository>().To<PostRepository>();
        }
    }
}